﻿namespace Calculadora
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button_resultado = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_operacao = new System.Windows.Forms.TextBox();
            this.textBox_valor2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_resultado = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_valor1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_resultado
            // 
            this.button_resultado.Location = new System.Drawing.Point(175, 182);
            this.button_resultado.Name = "button_resultado";
            this.button_resultado.Size = new System.Drawing.Size(75, 23);
            this.button_resultado.TabIndex = 0;
            this.button_resultado.Text = "=";
            this.button_resultado.UseVisualStyleBackColor = true;
            this.button_resultado.Click += new System.EventHandler(this.button_resultado_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "operacao";
            // 
            // textBox_operacao
            // 
            this.textBox_operacao.Location = new System.Drawing.Point(159, 122);
            this.textBox_operacao.Name = "textBox_operacao";
            this.textBox_operacao.Size = new System.Drawing.Size(100, 20);
            this.textBox_operacao.TabIndex = 2;
            // 
            // textBox_valor2
            // 
            this.textBox_valor2.Location = new System.Drawing.Point(159, 156);
            this.textBox_valor2.Name = "textBox_valor2";
            this.textBox_valor2.Size = new System.Drawing.Size(100, 20);
            this.textBox_valor2.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(100, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "valor2";
            // 
            // textBox_resultado
            // 
            this.textBox_resultado.Location = new System.Drawing.Point(159, 211);
            this.textBox_resultado.Name = "textBox_resultado";
            this.textBox_resultado.Size = new System.Drawing.Size(100, 20);
            this.textBox_resultado.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Resultado";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // textBox_valor1
            // 
            this.textBox_valor1.Location = new System.Drawing.Point(159, 80);
            this.textBox_valor1.Name = "textBox_valor1";
            this.textBox_valor1.Size = new System.Drawing.Size(100, 20);
            this.textBox_valor1.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(100, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "valor1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(361, 254);
            this.Controls.Add(this.textBox_valor1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_resultado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_valor2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_operacao);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_resultado);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_resultado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_operacao;
        private System.Windows.Forms.TextBox textBox_valor2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_resultado;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_valor1;
        private System.Windows.Forms.Label label4;
    }
}

