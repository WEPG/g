﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button_resultado_Click(object sender, EventArgs e)
        {
            int num1;
            int num2;
            int resultado;

            num1 = Convert.ToInt32(textBox_valor1.Text);
            num2 = Convert.ToInt32(textBox_valor2.Text);

            switch(textBox_operacao.Text)
            {
                case "+":
                  resultado = num1 + num2;
                    textBox_resultado.Text = resultado.ToString();
                    break;
               
                case "-":
                    resultado = num1 - num2;
                    textBox_resultado.Text = resultado.ToString();
                    break;
                
                case "/":                  
                    if (num2 == 0)
                    MessageBox.Show("Divisão por zero", "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    resultado = num1 / num2;
                    textBox_resultado.Text = resultado.ToString();
                    break;
                    if (resultado == 2)
                        return;

                case "*":
                    resultado = num1 * num2;
                    textBox_resultado.Text = resultado.ToString();
                    break;
               
                default: 
                    MessageBox.Show("Operador invalido", "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
            }

        }
    }
}
